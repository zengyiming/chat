defmodule Chat.Supervisor do
  use Supervisor

  def start_link(_args) do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end
  def init(:ok) do
    children = [
      {DynamicSupervisor, name: Chat.RoomSupervisor, strategy: :one_for_one},
      {DynamicSupervisor, name: Chat.UserSupervisor, strategy: :one_for_one},
      %{
        id: Chat.RoomManager,
        start: {Chat.RoomManager, :start_link, []},
        type: :worker
      },
      %{
        id: Chat.UserManager,
        start: {Chat.UserManager, :start_link, []},
        type: :worker
      },
      worker(Mongo, [[name: :mongo, database: "chat", pool_size: 10]]),
      Supervisor.child_spec(
        {
          Brotorift.Supervisor,
          [port: 4040, mod: Chat.ChatServerConnection, handler: Chat.Server, data_head: 1000, hb_timeout: 6000000]
        },
        id: :brotorift
      ),
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
