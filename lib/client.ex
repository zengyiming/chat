defmodule Chat.Client do
  use GenServer
  require Logger
  @behaviour :ranch_protocol
  @doc """
    1. Interfaces
      1. For OTP callbacks
      2. Helpers
    2. OTP callbacks
    3. Custom fuctions defp
  """
  # def start_link(socket) do
  #   GenServer.start_link(__MODULE__, socket)
  # end

  def start_link(ref, socket, transport, _opts) do
    pid = :proc_lib.spawn_link(__MODULE__, :init, [ref, socket, transport])
    {:ok, pid}
  end

  # @spec deliver_msg(atom | pid | {atom, any} | {:via, atom, any}, any) :: :ok
  # def deliver_msg(client_pid, msg) do
  #   GenServer.cast(client_pid, {:deliver_msg, msg})
  # end

  def send_msg_to_client(client_pid, msg) do
    GenServer.cast(client_pid, {:send_msg_to_client, msg})
  end

  def init(_args)do
    {:ok, {}}
  end

  def init(ref, socket, transport) do
    peername = stringify_peername(socket)

    Logger.info("Client #{peername} connecting")

    :ok = :ranch.accept_ack(ref)
    :ok = transport.setopts(socket, [{:active, true}])

    :gen_server.enter_loop(__MODULE__, [], %{
      socket: socket,
      transport: transport,
      client_name: peername,
      self_pid: self()
    })
  end
  # def init(socket) do
  #   pid = self()
  #   {:ok, serve_pid} = Task.start_link(fn  -> serve(pid, socket) end)
  #   {:ok, %{self_pid: pid, serve_pid: serve_pid, socket: socket}}
  # end

  # def handle_cast({:deliver_msg, msg}, client) do
  #   result = exec_command(client.self_pid, msg)
  #   write_line(client.socket, result)
  #   {:noreply, client}
  # end

  def handle_cast({:send_msg_to_client, msg}, client) do
    client.transport.send(client.socket, msg)
    {:noreply, client}
  end

  def handle_info({:tcp, _, message}, client) do
    Logger.info("Received new message from client #{client.client_name}: #{inspect(message)}")

    result = exec_command(client.self_pid, message)
    client.transport.send(client.socket, result)
    {:noreply, client}
  end

  def handle_info({:tcp_closed, _}, %{client_name: client_name} = state) do
    Logger.info(fn ->
      "Client #{client_name} disconnected"
    end)

    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, _, reason}, %{client_name: client_name} = state) do
    Logger.info(fn ->
      "Error with client #{client_name}: #{inspect(reason)}"
    end)

    {:stop, :normal, state}
  end

  # defp serve(client_pid, socket) do
  #   case read_line(socket) do
  #     {:ok, msg} ->
  #       Chat.Client.deliver_msg(client_pid, msg)
  #       serve(client_pid, socket)
  #   end
  # end

  defp exec_command(client_pid, msg) do
    # Logger.info(msg)
    trimed_msg = String.trim(msg)
    case String.split(trimed_msg, " ") do
      [cmd] ->
        case cmd do
          "SHOW_ROOMS" ->
            room_names = Chat.RoomManager.get_room_names()
            Enum.reduce(room_names, "\nRoom list:\n", fn room_name, result -> result <> room_name <> "\n"  end)

          _ ->
            "Invalid message\n"
        end

      [cmd, arg] ->
        case cmd do
          "CREATE_ROOM" ->
            Chat.RoomManager.create_room(arg)
            "Create room <#{arg}> success\n"

          "CREATE_USER" ->
            Chat.UserManager.create_user(arg)
            "Create user <#{arg}> success\n"

          "LOGIN" ->
            case Chat.UserManager.get_user_by_name(arg) do
              nil -> "User <#{arg}> not exist\n"
              user -> Chat.User.connect_client(user.pid, client_pid)
                      "Login user <#{arg}> success\n"
            end

            _ ->
              "Invalid message\n"
        end

      [cmd, arg1, arg2] ->
        case cmd do
          "SEND" ->
            user = Chat.UserManager.get_user_by_name(arg1)
            Chat.User.send(user.pid, arg2)
            user_detail = Chat.User.lookup(user.pid)
            msg_to_forward = "User <#{arg1}> Send <#{arg2}> \n"
            forward_msg(user_detail.room_pid, msg_to_forward)
            ""

          "JOIN" ->
            user = Chat.UserManager.get_user_by_name(arg2)
            room = Chat.RoomManager.get_room_by_name(arg1)
            # IO.inspect(user)
            # IO.inspect(room)
            Chat.User.join(room.pid, user.pid)
            user_detail = Chat.User.lookup(user.pid)
            msg_to_forward = "User <#{arg2}> join room <#{arg1}> \n"
            forward_msg(user_detail.room_pid, msg_to_forward)
            ""

            _ ->
              "Invalid message\n"
        end
      _ ->
        "Invalid message\n"
      end
  end

  defp forward_msg(room_pid, msg) do
    room = Chat.Room.lookup(room_pid)
    user_pid_list = room.user_pid_list
    Enum.each(user_pid_list, fn user_pid ->
      user = Chat.User.lookup(user_pid)
      if user.client_pid != nil do
        Chat.Client.send_msg_to_client(user.client_pid, msg)
      end
    end)
  end

  # defp read_line(socket) do
  #   :gen_tcp.recv(socket, 0)
  # end

  # defp write_line(socket, text) do
  #   :gen_tcp.send(socket, text)
  # end

  defp stringify_peername(socket) do
    {:ok, {addr, port}} = :inet.peername(socket)

    address =
      addr
      |> :inet_parse.ntoa()
      |> to_string()

    "#{address}:#{port}"
  end
end
