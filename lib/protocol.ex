defmodule Chat.Enums do
end


defmodule Chat.RoomInfo do
  defstruct [:name, :id, :max_number, :current_number]

  import Chat.Enums

  @typedoc """

  `:name`:
  `:id`:
  `:max_number`:
  `:current_number`:
  """
  @type t :: %Chat.RoomInfo{name: String.t(), id: non_neg_integer(), max_number: non_neg_integer(), current_number: non_neg_integer()}

  def read(data) do
    {data, name} = Brotorift.Binary.read_string(data)
    {data, id} = Brotorift.Binary.read_uint(data)
    {data, max_number} = Brotorift.Binary.read_uint(data)
    {data, current_number} = Brotorift.Binary.read_uint(data)
    {data, %Chat.RoomInfo{name: name, id: id, max_number: max_number, current_number: current_number}}
  end

  def write(data, value) do
    data = Brotorift.Binary.write_string(data, value.name)
    data = Brotorift.Binary.write_uint(data, value.id)
    data = Brotorift.Binary.write_uint(data, value.max_number)
    data = Brotorift.Binary.write_uint(data, value.current_number)
    data
  end
end

defmodule Chat.UserColor do
  defstruct [:h, :s, :v]

  import Chat.Enums

  @typedoc """

  `:h`:
  `:s`:
  `:v`:
  """
  @type t :: %Chat.UserColor{h: float(), s: float(), v: float()}

  def read(data) do
    {data, h} = Brotorift.Binary.read_float(data)
    {data, s} = Brotorift.Binary.read_float(data)
    {data, v} = Brotorift.Binary.read_float(data)
    {data, %Chat.UserColor{h: h, s: s, v: v}}
  end

  def write(data, value) do
    data = Brotorift.Binary.write_float(data, value.h)
    data = Brotorift.Binary.write_float(data, value.s)
    data = Brotorift.Binary.write_float(data, value.v)
    data
  end
end

defmodule Chat.UserInfo do
  defstruct [:name, :id, :color]

  import Chat.Enums

  @typedoc """

  `:name`:
  `:id`:
  `:color`:
  """
  @type t :: %Chat.UserInfo{name: String.t(), id: non_neg_integer(), color: Chat.UserColor.t}

  def read(data) do
    {data, name} = Brotorift.Binary.read_string(data)
    {data, id} = Brotorift.Binary.read_uint(data)
    {data, color} = Chat.UserColor.read(data)
    {data, %Chat.UserInfo{name: name, id: id, color: color}}
  end

  def write(data, value) do
    data = Brotorift.Binary.write_string(data, value.name)
    data = Brotorift.Binary.write_uint(data, value.id)
    data = Chat.UserColor.write(data, value.color)
    data
  end
end

defmodule Chat.ChatLog do
  defstruct [:user_info, :text, :date]

  import Chat.Enums

  @typedoc """

  `:user_info`:
  `:text`:
  `:date`:
  """
  @type t :: %Chat.ChatLog{user_info: Chat.UserInfo.t, text: String.t(), date: String.t()}

  def read(data) do
    {data, user_info} = Chat.UserInfo.read(data)
    {data, text} = Brotorift.Binary.read_string(data)
    {data, date} = Brotorift.Binary.read_string(data)
    {data, %Chat.ChatLog{user_info: user_info, text: text, date: date}}
  end

  def write(data, value) do
    data = Chat.UserInfo.write(data, value.user_info)
    data = Brotorift.Binary.write_string(data, value.text)
    data = Brotorift.Binary.write_string(data, value.date)
    data
  end
end



defmodule Chat.ChatServerConnection do
  use GenServer, restart: :temporary

  @version 9

  @sc_packet_data 128

  import Chat.Enums

  @behaviour Brotorift.ConnectionBehaviour

  @header_login 1001
  @header_create_user 1002
  @header_create_room 1003
  @header_send 1004
  @header_join 1005
  @header_leave 1006
  @header_get_room_info 1007
  @header_show_rooms 1008
  @header_get_room_logs 1009

  @header_login_response 2001
  @header_create_user_response 2002
  @header_create_room_response 2003
  @header_send_response 2004
  @header_join_response 2005
  @header_leave_response 2006
  @header_get_room_info_response 2007
  @header_show_rooms_response 2008
  @header_update_room_list 2009
  @header_update_room_info 2010
  @header_get_room_logs_response 2011
  @header_send_message 2012

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `response`:

    - `user_info`:

  """
  @spec login_response(connection :: pid(), response :: String.t(), user_info :: Chat.UserInfo.t) :: :ok
  def login_response(connection, response, user_info) do
    GenServer.cast(connection, {:login_response, response, user_info})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `response`:

  """
  @spec create_user_response(connection :: pid(), response :: String.t()) :: :ok
  def create_user_response(connection, response) do
    GenServer.cast(connection, {:create_user_response, response})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `response`:

  """
  @spec create_room_response(connection :: pid(), response :: String.t()) :: :ok
  def create_room_response(connection, response) do
    GenServer.cast(connection, {:create_room_response, response})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `response`:

  """
  @spec send_response(connection :: pid(), response :: String.t()) :: :ok
  def send_response(connection, response) do
    GenServer.cast(connection, {:send_response, response})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `response`:

  """
  @spec join_response(connection :: pid(), response :: String.t()) :: :ok
  def join_response(connection, response) do
    GenServer.cast(connection, {:join_response, response})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `response`:

  """
  @spec leave_response(connection :: pid(), response :: String.t()) :: :ok
  def leave_response(connection, response) do
    GenServer.cast(connection, {:leave_response, response})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `room_info`:

  """
  @spec get_room_info_response(connection :: pid(), room_info :: Chat.RoomInfo.t) :: :ok
  def get_room_info_response(connection, room_info) do
    GenServer.cast(connection, {:get_room_info_response, room_info})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `room_list`:

  """
  @spec show_rooms_response(connection :: pid(), room_list :: list(Chat.RoomInfo.t)) :: :ok
  def show_rooms_response(connection, room_list) do
    GenServer.cast(connection, {:show_rooms_response, room_list})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `room_list`:

  """
  @spec update_room_list(connection :: pid(), room_list :: list(Chat.RoomInfo.t)) :: :ok
  def update_room_list(connection, room_list) do
    GenServer.cast(connection, {:update_room_list, room_list})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `room_info`:

  """
  @spec update_room_info(connection :: pid(), room_info :: Chat.RoomInfo.t) :: :ok
  def update_room_info(connection, room_info) do
    GenServer.cast(connection, {:update_room_info, room_info})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `roomname`:

    - `chat_log_list`:

  """
  @spec get_room_logs_response(connection :: pid(), roomname :: String.t(), chat_log_list :: list(Chat.ChatLog.t)) :: :ok
  def get_room_logs_response(connection, roomname, chat_log_list) do
    GenServer.cast(connection, {:get_room_logs_response, roomname, chat_log_list})
  end

  @doc """


  ## Parameters

    - `connection`: ChatServerConnection Pid
    - `room_id`:

    - `log`:

  """
  @spec send_message(connection :: pid(), room_id :: non_neg_integer(), log :: Chat.ChatLog.t) :: :ok
  def send_message(connection, room_id, log) do
    GenServer.cast(connection, {:send_message, room_id, log})
  end

  def version() do
    @version
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def handle_data(pid, data) do
    GenServer.cast(pid, {:handle_data, data})
  end

  def stop(pid) do
    GenServer.cast(pid, :stop)
  end

  def init({socket, transport, handler}) do
    {:ok, state} = handler.open_connection(self(), socket)
    {:ok, {socket, transport, handler, state}}
  end

  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end

  def handle_cast({:handle_data, data}, {socket, transport, handler, state}) do
    result = process_packet(data, handler, state)
    case result do
      {:ok, new_state} ->
        {:noreply, {socket, transport, handler, new_state}}
      {:stop, reason, new_state} ->
        {:stop, reason, {socket, transport, handler, new_state}}
    end
  end

  def handle_cast({:login_response, response, user_info}, {socket, transport, handler, state}) do
    data = <<@header_login_response::32-little>>
    data = Brotorift.Binary.write_string(data, response)
    data = Chat.UserInfo.write(data, user_info)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:create_user_response, response}, {socket, transport, handler, state}) do
    data = <<@header_create_user_response::32-little>>
    data = Brotorift.Binary.write_string(data, response)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:create_room_response, response}, {socket, transport, handler, state}) do
    data = <<@header_create_room_response::32-little>>
    data = Brotorift.Binary.write_string(data, response)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:send_response, response}, {socket, transport, handler, state}) do
    data = <<@header_send_response::32-little>>
    data = Brotorift.Binary.write_string(data, response)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:join_response, response}, {socket, transport, handler, state}) do
    data = <<@header_join_response::32-little>>
    data = Brotorift.Binary.write_string(data, response)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:leave_response, response}, {socket, transport, handler, state}) do
    data = <<@header_leave_response::32-little>>
    data = Brotorift.Binary.write_string(data, response)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:get_room_info_response, room_info}, {socket, transport, handler, state}) do
    data = <<@header_get_room_info_response::32-little>>
    data = Chat.RoomInfo.write(data, room_info)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:show_rooms_response, room_list}, {socket, transport, handler, state}) do
    data = <<@header_show_rooms_response::32-little>>
    data = Brotorift.Binary.write_list(data, room_list, &Chat.RoomInfo.write/2)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:update_room_list, room_list}, {socket, transport, handler, state}) do
    data = <<@header_update_room_list::32-little>>
    data = Brotorift.Binary.write_list(data, room_list, &Chat.RoomInfo.write/2)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:update_room_info, room_info}, {socket, transport, handler, state}) do
    data = <<@header_update_room_info::32-little>>
    data = Chat.RoomInfo.write(data, room_info)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:get_room_logs_response, roomname, chat_log_list}, {socket, transport, handler, state}) do
    data = <<@header_get_room_logs_response::32-little>>
    data = Brotorift.Binary.write_string(data, roomname)
    data = Brotorift.Binary.write_list(data, chat_log_list, &Chat.ChatLog.write/2)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def handle_cast({:send_message, room_id, log}, {socket, transport, handler, state}) do
    data = <<@header_send_message::32-little>>
    data = Brotorift.Binary.write_uint(data, room_id)
    data = Chat.ChatLog.write(data, log)
    data = <<@sc_packet_data::8, byte_size(data)::32-little, data::binary>>
    transport.send(socket, data)
    {:noreply, {socket, transport, handler, state}}
  end

  def terminate(_reason, {socket, transport, handler, state}) do
    handler.close_connection(self(), state)
    transport.close(socket)
  end

  defp process_packet(<<>>, _handler, state) do
    {:ok, state}
  end
  defp process_packet(data, handler, state) do
    <<header::32-little, data::binary>> = data
    case header do
      @header_login ->
        {data, username} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.login(self(), state, username)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_create_user ->
        {data, username} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.create_user(self(), state, username)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_create_room ->
        {data, roomname} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.create_room(self(), state, roomname)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_send ->
        {data, username} = Brotorift.Binary.read_string(data)
        {data, msg} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.send(self(), state, username, msg)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_join ->
        {data, roomname} = Brotorift.Binary.read_string(data)
        {data, username} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.join(self(), state, roomname, username)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_leave ->
        {data, roomname} = Brotorift.Binary.read_string(data)
        {data, username} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.leave(self(), state, roomname, username)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_get_room_info ->
        {data, roomname} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.get_room_info(self(), state, roomname)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_show_rooms ->
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.show_rooms(self(), state)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
      @header_get_room_logs ->
        {data, roomname} = Brotorift.Binary.read_string(data)
        if byte_size(data) != 0 do
          stop(self())
        else
          result = handler.get_room_logs(self(), state, roomname)
          case result do
            {:ok, state} ->
              process_packet(data, handler, state)
            {:stop, reason, new_state} ->
              {:stop, reason, new_state}
          end
        end
    end
  end
end


defmodule Chat.ChatServerBehaviour do

  @doc """
  Calls when the server started
  """
  @callback start() :: :ok

  @doc """
  Calls when a new client connects

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `socket`: The socket for the client

  ## Returns

    {:ok, state}

  """
  @callback open_connection(connection :: pid(), socket :: :gen_tcp.socket()) :: {:ok, any()}

  @doc """
  Calls when a client disconnects

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection

  """
  @callback close_connection(connection :: pid(), state :: any()) :: :ok


  @doc """
  Login with username

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `username`:

  """
  @callback login(connection :: pid(), state :: any(), username :: String.t()) :: {:ok, any()}

  @doc """
  Create user with username

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `username`:

  """
  @callback create_user(connection :: pid(), state :: any(), username :: String.t()) :: {:ok, any()}

  @doc """
  Create chatroom with roomname

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `roomname`:

  """
  @callback create_room(connection :: pid(), state :: any(), roomname :: String.t()) :: {:ok, any()}

  @doc """
  Send message with username

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `username`:

    - `msg`:

  """
  @callback send(connection :: pid(), state :: any(), username :: String.t(), msg :: String.t()) :: {:ok, any()}

  @doc """
  Let user join target room

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `roomname`:

    - `username`:

  """
  @callback join(connection :: pid(), state :: any(), roomname :: String.t(), username :: String.t()) :: {:ok, any()}

  @doc """


  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `roomname`:

    - `username`:

  """
  @callback leave(connection :: pid(), state :: any(), roomname :: String.t(), username :: String.t()) :: {:ok, any()}

  @doc """


  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `roomname`:

  """
  @callback get_room_info(connection :: pid(), state :: any(), roomname :: String.t()) :: {:ok, any()}

  @doc """
  Show room list

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
  """
  @callback show_rooms(connection :: pid(), state :: any()) :: {:ok, any()}

  @doc """
  Get target room logs

  ## Parameters

    - `connection`: The ChatServerConnection Pid for the client
    - `state`: The state for the connection
    - `roomname`:

  """
  @callback get_room_logs(connection :: pid(), state :: any(), roomname :: String.t()) :: {:ok, any()}

end


