defmodule Chat.Room do
  use GenServer
  @doc """
    1. Interfaces
      1. For OTP callbacks
      2. Helpers
    2. OTP callbacks
    3. Custom fuctions defp
  """

  # Interfaces
  def start_link(args) do
    GenServer.start_link(
      __MODULE__,
      args
    )
  end

  def lookup(pid) do
    GenServer.call(pid, {:lookup})
  end

  def get_log(pid) do
    GenServer.call(pid, {:get_log})
  end

  def add_log(pid, user_name, text) do
    GenServer.call(pid, {:add_log, user_name, text})
  end

  def join(pid, user_pid) do
    GenServer.call(pid, {:join, user_pid})
  end

  def leave(pid, user_pid) do
    GenServer.call(pid, {:leave, user_pid})
  end

  # OTP callbacks
  def init(args) do
    # IO.inspect(args)
    {:ok, %{name: args.name, id: args.id, max_number: args.max_number, current_number: args.current_number, chat_log: args.chat_log, user_pid_list: []}}
  end

  def handle_call({:lookup}, _from, room) do
    {:reply, room, room}
  end

  def handle_call({:get_log}, _from, room) do
    {:reply, room.chat_log, room}
  end

  def handle_call({:join, user_pid}, _from, room) do
    case Enum.member?(room.user_pid_list, user_pid) do
      false -> new_user_pid_list = [user_pid | room.user_pid_list]
               new_room = %{room | user_pid_list: new_user_pid_list, current_number: room.current_number + 1}
               {:reply, new_room, new_room}
      true -> {:reply, :already_exist, room}
    end
  end

  def handle_call({:leave, user_pid}, _from, room) do
    case Enum.member?(room.user_pid_list, user_pid) do
      true -> new_user_pid_list = List.delete(room.user_pid_list, user_pid)
               new_room = %{room | user_pid_list: new_user_pid_list, current_number: room.current_number - 1}
               {:reply, new_room, new_room}
      false -> {:reply, :not_exist, room}
    end
  end

  def handle_call({:add_log, user_name, text}, _from, room) do
    # IO.puts text
    {:ok, utc_now} = DateTime.now("Etc/UTC")
    bj_now = DateTime.add(utc_now, 3600*8)
    date_str = DateTime.to_string(bj_now)

    user = Chat.UserManager.get_user_by_name(user_name)
    user_info = %Chat.UserInfo{name: user_name, id: user.id, color: %Chat.UserColor{h: user.color.h, s: user.color.s, v: user.color.v}}
    log = %Chat.ChatLog{user_info: user_info, text: text, date: date_str}
    new_log = [log | room.chat_log]
    new_room = %{room | chat_log: new_log}

    Chat.RoomManager.add_log_to_db(room.name, log)
    {:reply, log, new_room}
  end
end
