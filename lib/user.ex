defmodule Chat.User do
  use GenServer
  @doc """
    1. Interfaces
      1. For OTP callbacks
      2. Helpers
    2. OTP callbacks
    3. Custom fuctions defp
  """

  # Interfaces
  def start_link(name) do
    GenServer.start_link(__MODULE__, name)
  end

  def lookup(pid) do
    GenServer.call(pid, {:lookup})
  end

  def join(room_pid, user_pid) do
    GenServer.call(user_pid, {:join, room_pid, user_pid})
  end

  def leave(room_pid, user_pid) do
    GenServer.call(user_pid, {:leave, room_pid, user_pid})
  end

  def send(user_pid, text) do
    GenServer.call(user_pid, {:send, text})
  end

  def show_log(user_pid) do
    GenServer.cast(user_pid, {:show_log})
  end

  def connect_client(user_pid, client_pid) do
    GenServer.cast(user_pid, {:connect_client, client_pid})
  end

  def disconnect_client(user_pid) do
    GenServer.cast(user_pid, {:disconnect_client})
  end

  # OTP callbacks
  def init(name) do
    {:ok, %{name: name, id: 0, room_pid: nil, client_pid: nil}}
  end

  def handle_call({:lookup}, _from, user) do
    {:reply, user, user}
  end

  def handle_call({:join, room_pid, user_pid}, _from, user) do
    case Chat.Room.join(room_pid, user_pid) do
      :already_join -> {:reply, :already_exist, user}
      _ -> new_user = %{user | room_pid: room_pid}
           {:reply, :success, new_user}
    end
  end

  def handle_call({:leave, room_pid, user_pid}, _from, user) do
    case user.room_pid do
      nil ->
        {:reply, :success, user}
      _ ->
        Chat.Room.leave(room_pid, user_pid)
        new_user = %{user | room_pid: nil}
        {:reply, :success, new_user}
    end
  end

  def handle_call({:send, text}, _from, user) do
    log = Chat.Room.add_log(user.room_pid, user.name, text)
    {:reply, log, user}
  end

  def handle_cast({:connect_client, client_pid}, user) do
    new_user = %{user | client_pid: client_pid}
    {:noreply, new_user}
  end

  def handle_cast({:disconnect_client}, user) do
    new_user = %{user | client_pid: nil}
    {:noreply, new_user}
  end

  def handle_cast({:show_log}, user) do
    case user.room_pid do
      nil -> IO.puts("No room")
      _ ->  Chat.Room.get_log(user.room_pid)
            |> IO.puts()
    end
    {:noreply, user}
  end
end
