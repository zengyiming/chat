defmodule Chat.UserManager do
  use GenServer
  require Logger

  @user_collection "users"

  # Interfaces
  def start_link() do
    GenServer.start_link(__MODULE__, {}, name: __MODULE__)
  end

  def create_user(name) do
    GenServer.call(__MODULE__, {:create_user, name})
  end

  def connect_user(connection, user_pid) do
    GenServer.call(__MODULE__, {:connect_user, connection, user_pid})
  end

  def disconnect_user(connection) do
    GenServer.call(__MODULE__, {:disconnect_user, connection})
  end

  def show_users() do
    GenServer.cast(__MODULE__, {:show_users})
  end

  def get_user_by_name(name) do
    GenServer.call(__MODULE__, {:get_user_by_name, name})
  end

  def get_user_pid_by_name(name) do
    GenServer.call(__MODULE__, {:get_user_pid_by_name, name})
  end

  def get_user_pid_by_connection(connection) do
    GenServer.call(__MODULE__, {:get_user_pid_by_connection, connection})
  end

  def get_all_connections() do
    GenServer.call(__MODULE__, {:get_all_connections})
  end

  #OTP callbacks
  def init(_) do
    {:ok, %{current_id: 0, map_connection: %{}, map_ref: %{}, map_user: %{}}}
  end

  def handle_call({:create_user, name}, _from, global) do
    is_user_exist = check_username_exist?(name)

    user_info_map =
      if is_user_exist do
        read_user_from_db(name)
      else
        new_id = global.current_id + 1
        map = %{name: name, color: %{h: :rand.uniform(), s: 0.7, v: 0.8}, id: new_id}
        save_user_to_db(map)
        map
      end

    {:ok, user_pid} = DynamicSupervisor.start_child(
      Chat.UserSupervisor,
      %{
          id: Chat.User,
          start: {Chat.User, :start_link, [name]},
          restart: :temporary
      }
    )
    ref = Process.monitor(user_pid)

    new_map_user = Map.put(global.map_user, user_pid, Map.put(user_info_map, :pid, user_pid))
    new_map_ref = Map.put(global.map_ref, ref, user_pid )
    new_global = %{current_id: user_info_map.id, map_connection: global.map_connection, map_user: new_map_user, map_ref: new_map_ref}

    case is_user_exist do
      true -> {:reply, {:already_exist, user_pid}, new_global}
      false -> {:reply, {:ok, user_pid}, new_global}
    end
  end

  def handle_call({:get_user_by_name, name}, _from, global) do
    user = Enum.find(global.map_user, fn {_key, value} -> value.name == name end)
    case user do
      nil -> {:reply, nil, global}
      _ -> {:reply, elem(user, 1), global}
    end
  end

  def handle_call({:get_user_pid_by_name, name}, _from, global) do
    user = Enum.find(global.map_user, fn {_key, value} -> value.name == name end)
    case user do
      nil -> {:reply, nil, global}
      _ -> {:reply, elem(user, 0), global}
    end
  end

  def handle_call({:get_user_pid_by_connection, connection}, _from, global) do
    user = Enum.find(global.map_connection, fn {key, _value} -> key == connection end)
    case user do
      nil -> {:reply, nil, global}
      _ -> {:reply, elem(user, 1), global}
    end
  end

  def handle_call({:get_all_connections}, _from, global) do
    list = Enum.reduce(global.map_connection, [], fn {key, _value}, acc -> [key | acc] end)
    {:reply, list, global}
  end

  def handle_call({:connect_user, connection, user_pid}, _from, global) do
    user = Enum.find(global.map_user, fn {key, _value} -> key == user_pid end)
    case user do
      nil ->
        {:reply, nil, global}
      _ ->
      # user_pid = elem(user, 0)
        Chat.User.connect_client(user_pid, connection)
        new_map_connection = Map.put(global.map_connection, connection, user_pid)
        new_global = Map.put(global, :map_connection, new_map_connection)
        {:reply, :ok, new_global}
    end
  end

  def handle_call({:disconnect_user, connection}, _from, global) do
    item = Enum.find(global.map_connection, fn {key, _value} -> key == connection end)
    case item do
      nil ->
        {:reply, nil, global}
      _ ->
        user_pid = elem(item, 1)
        Chat.User.disconnect_client(user_pid)
        new_map_connection = Map.delete(global.map_connection, connection)
        new_global = Map.put(global, :map_connection, new_map_connection)

        {:reply, :ok, new_global}
    end
  end

  def handle_cast({:show_users}, global) do
    Logger.info("Users: ")
    Enum.each(global.map_ref, fn{_ref, user_pid}->
      user = Chat.User.lookup(user_pid)
      Logger.info("-[name #{user.name}]")
    end)

    {:noreply, global}
  end

  def handle_info({:DOWN, ref, :process, _user_pid, _reason}, global) do
    # Process.demonitor(ref)
    {:ok, pid} = Map.fetch(global.map_ref, ref)
    new_map_ref = Map.delete(global.map_ref, ref)
    new_map_user = Map.delete(global.map_user, pid)
    new_global = %{current_id: global.current_id, map_user: new_map_user, map_ref: new_map_ref}
    {:noreply, new_global}
  end

  defp check_username_exist?(name) do
    case Mongo.find_one(:mongo, @user_collection, %{name: name}) do
      nil -> false
      _ -> true
    end
  end

  defp save_user_to_db(user_info_map) do
    _result = Mongo.insert_one(:mongo, @user_collection, user_info_map)
  end

  defp read_user_from_db(name) do
    Mongo.find_one(:mongo, @user_collection, %{name: name}, projection: %{_id: false})
      |> Poison.encode!
      |> Poison.Parser.parse!(%{keys: :atoms})
  end
end
