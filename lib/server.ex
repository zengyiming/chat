defmodule Chat.Server do
  @behaviour Chat.ChatServerBehaviour
  require Logger

  def start() do
    Logger.info("Server start")

    Chat.RoomManager.init_data_from_db()
  end

  def open_connection(_connection, _socket) do
    Logger.info("Client connect")
    {:ok, %{}}
  end

  def close_connection(connection, _state) do
    Logger.info("Client disconnect")
    user_pid = Chat.UserManager.get_user_pid_by_connection(connection)
    case user_pid do
      nil ->
        :ok
      _ ->
        user = Chat.User.lookup(user_pid)
        leave_by_pid(user.room_pid, user_pid)
        Chat.UserManager.disconnect_user(connection)
        :ok
    end
  end

  def login(connection, state, username) do
    case Chat.UserManager.get_user_by_name(username) do
      nil ->
        msg = "User <#{username}> not exist"
        Logger.info(msg)

        # user_info = %Chat.UserInfo{name: "NONE", id: 0, color: %Chat.UserColor{h: 0, s: 0, v: 0}}
        # Chat.ChatServerConnection.login_response(connection, "NOT_EXIST", user_info)
      user ->
        Chat.UserManager.connect_user(connection, user.pid)
        msg = "User <#{username}> login success"
        Logger.info(msg)

        user = Chat.UserManager.get_user_by_name(username)
        user_info = %Chat.UserInfo{name: username, id: user.id, color: %Chat.UserColor{h: user.color.h, s: user.color.s, v: user.color.v}}
        Chat.ChatServerConnection.login_response(connection, "SUCCESS", user_info)
    end
    {:ok, state}
  end

  def create_user(connection, state, username) do
    case Chat.UserManager.create_user(username) do
      {:already_exist, _user_pid} ->
        Logger.info("User <#{username}> already exist")
        Chat.ChatServerConnection.create_user_response(connection, "ALREADY_EXIST")
        {:ok, state}
      {:ok, _user_pid} ->
        Logger.info("Create user <#{username}>")
        Chat.ChatServerConnection.create_user_response(connection, "SUCCESS")
        {:ok, state}
    end
  end

  def create_room(connection, state, roomname) do
    case Chat.RoomManager.create_room(roomname) do
      {:already_exist, _room_pid} ->
        Logger.info("Room <#{roomname}> already exist")
        Chat.ChatServerConnection.create_room_response(connection, "ALREADY_EXIST")
        {:ok, state}
      {:ok, _room_pid} ->
        Logger.info("Create room <#{roomname}>")
        Chat.ChatServerConnection.create_room_response(connection, "SUCCESS")
        Chat.UserManager.get_all_connections()
        |> Enum.each(fn connection ->
              Chat.ChatServerConnection.update_room_list(connection, Chat.RoomManager.get_rooms())
            end)
        {:ok, state}
    end
  end

  def join(connection, state, roomname, username) do
    user = Chat.UserManager.get_user_pid_by_name(username)
    room = Chat.RoomManager.get_room_pid_by_name(roomname)
    Chat.User.join(room, user)

    # user_detail = Chat.User.lookup(user)
    msg_to_forward = "User <#{username}> join room <#{roomname}>"
    Logger.info(msg_to_forward)

    log = Chat.User.send(user, msg_to_forward)
    forward_msg(room, log)

    update_room_info_for_all_connections(room)
    Chat.ChatServerConnection.join_response(connection, "")

    {:ok, state}
  end

  def leave(connection, state, roomname, username) do
    user = Chat.UserManager.get_user_pid_by_name(username)
    room = Chat.RoomManager.get_room_pid_by_name(roomname)

    leave_by_pid(room, user)

    Chat.ChatServerConnection.leave_response(connection, "")
    {:ok, state}
  end

  defp leave_by_pid(room, user)
    when not is_nil(room) and not is_nil(user)
  do
    username = Chat.User.lookup(user).name
    roomname = Chat.Room.lookup(room).name
    msg_to_forward = "User <#{username}> leave room <#{roomname}>"

    Logger.info(msg_to_forward)
    log = Chat.User.send(user, msg_to_forward)
    forward_msg(room, log)

    Chat.User.leave(room, user)
    update_room_info_for_all_connections(room)

    :ok
  end
  defp leave_by_pid(_room, _user) do
    :nil_pid
  end

  def send(connection, state, username, msg) do
    user = Chat.UserManager.get_user_by_name(username)
    log = Chat.User.send(user.pid, msg)

    user_detail = Chat.User.lookup(user.pid)
    msg_to_forward = "User <#{username}> Send <#{msg}>"
    Logger.info(msg_to_forward)
    forward_msg(user_detail.room_pid, log)

    Chat.ChatServerConnection.send_response(connection, "")
    {:ok, state}
  end

  def show_rooms(connection, state) do
    Logger.info("Show room list")
    rooms = Chat.RoomManager.get_rooms()
    response = Enum.reduce(rooms, [], fn room, acc ->
      roomInfo = %Chat.RoomInfo{name: room.name, id: room.id, max_number: room.max_number, current_number: room.current_number}
      [roomInfo | acc]
    end)

    Chat.ChatServerConnection.show_rooms_response(connection, response)
    {:ok, state}
  end

  def get_room_logs(connection, state, room_name) do
    Logger.info("Get room #{room_name} logs")
    room = Chat.RoomManager.get_room_by_name(room_name)

    Chat.ChatServerConnection.get_room_logs_response(connection, room_name, room.chat_log)
    {:ok, state}
  end

  def get_room_info(connection, state, room_name) do
    Logger.info("Get room #{room_name} info")
    room = Chat.RoomManager.get_room_by_name(room_name)
    # IO.inspect(room)
    Chat.ChatServerConnection.get_room_info_response(
      connection,
      %Chat.RoomInfo{name: room.name, id: room.id, max_number: room.max_number, current_number: room.current_number}
      )
    {:ok, state}
  end

  defp forward_msg(room_pid, log) do
    room = Chat.Room.lookup(room_pid)
    user_pid_list = room.user_pid_list
    Enum.each(user_pid_list, fn user_pid ->
      user = Chat.User.lookup(user_pid)
      if user.client_pid != nil do
        Chat.ChatServerConnection.send_message(user.client_pid, room.id, log)
      end
    end)
  end

  defp update_room_info_for_all_connections(room_pid) do
    room_item = Chat.Room.lookup(room_pid)
    room_info = %Chat.RoomInfo{name: room_item.name, id: room_item.id, max_number: room_item.max_number, current_number: room_item.current_number}
    Chat.UserManager.get_all_connections()
    |> Enum.each(fn connection ->
          Chat.ChatServerConnection.update_room_info(connection, room_info)
        end)
  end
end
