defmodule Chat.RoomManager do
  use GenServer
  require Logger

  @room_collection "rooms"
  @global_collection "global"

  # Interfaces
  def start_link() do
    GenServer.start_link(__MODULE__, {}, name: __MODULE__)
  end

  def init_data_from_db() do
    init_global_from_db() # init global first
    init_rooms_from_db() # then create rooms
  end

  def create_room(name) do
    GenServer.call(__MODULE__, {:create_room, name})
  end

  def get_room_by_name(name) do
    GenServer.call(__MODULE__, {:get_room_by_name, name})
  end

  def get_room_by_id(id) do
    GenServer.call(__MODULE__, {:get_room_by_id, id})
  end

  def get_room_pid_by_name(name) do
    GenServer.call(__MODULE__, {:get_room_pid_by_name, name})
  end

  def get_rooms() do
    GenServer.call(__MODULE__, {:get_rooms})
  end

  def get_room_names() do
    GenServer.call(__MODULE__, {:get_room_names})
  end

  def add_log_to_db(room_name, log) do
    GenServer.cast(__MODULE__, {:add_log_to_db, room_name, log})
  end

  # OTP callbacks
  def init(_) do
    {:ok, %{current_id: 0, map_ref: %{}, map_room: %{}}}
  end

  def handle_call({:create_room, name}, _from, global) do
    is_room_exist = check_roomname_exist?(name)

    room_info_map =
      if is_room_exist do
        read_room_from_db(name)
      else
        new_id = global.current_id + 1
        map = %{name: name, id: new_id, max_number: 10000, current_number: 0, chat_log: []}
        save_room_to_db(map)
        map
      end

    {:ok, room_pid} = DynamicSupervisor.start_child(
      Chat.RoomSupervisor,
      %{
          id: Chat.Room,
          start: {Chat.Room, :start_link, [room_info_map]},
          restart: :temporary
      }
    )
    ref = Process.monitor(room_pid)

    new_map_room = Map.put(global.map_room, room_pid, Map.put_new(room_info_map, :pid, room_pid))
    new_map_ref = Map.put(global.map_ref, ref, room_pid )

    case is_room_exist do
      true ->
        new_global = %{current_id: global.current_id, map_room: new_map_room, map_ref: new_map_ref}
        {:reply, {:already_exist, room_pid}, new_global}
      false ->
        new_global = %{current_id: room_info_map.id, map_room: new_map_room, map_ref: new_map_ref}
        save_global_to_db(new_global)
        {:reply, {:ok, room_pid}, new_global}
    end
  end

  def handle_call({:get_room_by_name, name}, _from, global) do
    room = Enum.find(global.map_room, fn {_key, value} -> value.name == name end)

    case room do
      nil -> {:reply, nil, global}
      _ -> {:reply, Chat.Room.lookup(elem(room, 0)), global}
    end
  end

  def handle_call({:get_room_pid_by_name, name}, _from, global) do
    room = Enum.find(global.map_room, fn {_key, value} -> value.name == name end)

    case room do
      nil -> {:reply, nil, global}
      _ -> {:reply, elem(room, 0), global}
    end
  end

  def handle_call({:get_room_by_id, id}, _from, global) do
    room = Enum.find(global.map_room, fn {_key, value} -> value.id == id end)

    case room do
      nil -> {:reply, nil, global}
      _ -> {:reply, Chat.Room.lookup(elem(room, 0)), global}
    end
  end

  def handle_call({:get_room_names}, _from, global) do
    rooms =  Enum.reduce(global.map_ref, [], fn{_ref, room_pid}, room_names->
      room = Chat.Room.lookup(room_pid)
      [room.name | room_names]
    end)

    {:reply, rooms, global}
  end

  def handle_call({:init_global_from_db}, _from, global) do
    case read_global_from_db() do
      nil ->
        {:reply, :ok, global}
      global_db ->
        IO.inspect global_db
        {:reply, :ok, %{global | current_id: global_db["current_room_id"]}}
    end
  end

  def handle_call({:get_rooms}, _from, global) do
    rooms = Enum.reduce(global.map_room, [], fn{_, room}, acc -> [room | acc] end)
    {:reply, rooms, global}
  end

  def handle_cast({:add_log_to_db, room_name, log}, global) do
    room = read_room_from_db(room_name)
    new_logs = [log | room.chat_log]
    data = convert_data_from_struct_to_map(new_logs) # actually convert struct to map
    Mongo.update_one(:mongo, @room_collection, %{name: room_name}, %{"$set": %{chat_log: data}})
    {:noreply, global}
  end

  def handle_info({:DOWN, ref, :process, _room_pid, _reason}, global) do
    # Process.demonitor(ref)
    {:ok, pid} = Map.fetch(global.map_ref, ref)
    new_map_ref = Map.delete(global.map_ref, ref)
    new_map_room = Map.delete(global.map_id, pid)
    new_global = %{current_id: global.current_id, map_room: new_map_room, map_ref: new_map_ref}
    {:noreply, new_global}
  end

  defp init_rooms_from_db() do
    Mongo.find(:mongo, @room_collection, %{})
    |> Enum.to_list()
    |> Enum.each(fn room ->
      Chat.RoomManager.create_room(room["name"]) end)

    :ok
  end

  defp init_global_from_db() do
    GenServer.call(__MODULE__, {:init_global_from_db})
  end

  defp check_roomname_exist?(name) do
    doc = Mongo.find_one(:mongo, @room_collection, %{name: name})
    case doc do
      nil -> false
      _ -> true
    end
  end

  defp save_global_to_db(global) do
    case Mongo.find_one(:mongo, @global_collection, %{_id: "global"}) do
      nil ->
        Mongo.insert_one!(:mongo, @global_collection, %{_id: "global", current_room_id: global.current_id})
      _doc ->
        Mongo.update_one!(:mongo, @global_collection, %{_id: "global"}, %{"$set": %{current_room_id: global.current_id}})
    end
  end

  defp read_global_from_db() do
    Mongo.find_one(:mongo, @global_collection, %{_id: "global"})
  end

  defp save_room_to_db(room_data_map) do
    Mongo.insert_one!(:mongo, @room_collection, room_data_map)
  end

  defp read_room_from_db(name) do
    Mongo.find_one(:mongo, @room_collection, %{name: name}, projection: %{_id: false})
    |> Poison.encode!
    |> Poison.Parser.parse!(%{keys: :atoms})
  end

  defp convert_data_from_struct_to_map(data) do
    Poison.encode!(data)
    |> Poison.decode!()
  end
end
